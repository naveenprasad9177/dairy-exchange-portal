import React, { Component, Fragment } from "react";
import LoginForm from "../../Components/Login/Form";

export class Login extends Component {
  render() {
    return (
      <Fragment>
        <LoginForm />
      </Fragment>
    );
  }
}

export default Login;
