import React from "react";
import "./index.css";

export default function input(props) {
  return (
    <div className="flex-column mt-3">
      <label>{props.label}</label>
      <input
        className="mt-2"
        type={props.type}
        value={props.value}
        onChange={props.handleChange}
      />
    </div>
  );
}
