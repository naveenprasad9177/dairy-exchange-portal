import React, { Component } from "react";
import Input from "../Input";
import "./index.css";

export default class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "" };
  }

  handleEmailChange = event => {
    this.setState({ email: event.target.value });
  };
  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleSubmit = event => {
    alert("A name was submitted: " + this.state.email + this.state.password);
    event.preventDefault();
  };

  render() {
    return (
      <form className="form-container" onSubmit={this.handleSubmit}>
        <Input
          label="Email :"
          type="text"
          value={this.state.email}
          handleChange={this.handleEmailChange}
        ></Input>
        <Input
          label="Password:"
          type="password"
          value={this.state.password}
          handleChange={this.handlePasswordChange}
        ></Input>
        <button className="mt-3" type="submit">
          Submit
        </button>
      </form>
    );
  }
}
